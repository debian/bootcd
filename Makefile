all	: bootcd_*.deb

bootcd_*.deb	: \
	Makefile \
	src/bootcd \
	src/Makefile \
	src/debian/rules
	@if [ $(USER) = "root" ]; then echo "Makefile: do not run as root"; exit 1; fi
	@(cd src; debuild --lintian-opts -i -- binary)

clean	:
	@(cd src; debuild -- clean)
	@rm -f bootcd_*.build
	@rm -f bootcd_*.dsc
	@rm -f bootcd_*.changes
	@rm -f bootcd_*.tar.gz
	@rm -f bootcd_*.tar.xz
	@rm -f bootcd_*.buildinfo

clobber : clean
	@rm -f bootcd_*.deb
	@rm -rf /usr/local/src/Packages/bootcd/

install	: /var/lib/dpkg/info/bootcd.list

special : install
	@(cd src; ./run_tests -t special.test.mylib -t special.test2.mylib)

/var/lib/dpkg/info/bootcd.list: bootcd_*.deb
	@sudo dpkg --force-confmiss -i bootcd_*.deb
