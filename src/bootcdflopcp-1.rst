==============
 bootcdflopcp
==============

-----------------------------------------------------
copy changes made after booting from bootcd to floppy
-----------------------------------------------------

:Author: bernd.schumacher@hpe.com
:Date: 2020-08-07
:Copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
:License: GNU General Public License, version 3
:Version: 0.1
:Manual section: 1
:Manual group: bootcd utils

SYNOPSIS
========
*bootcdflopcp* [**-v**] [**-d** *device*]

DESCRIPTION
===========

**bootcd2disk** is used to write a running bootcd built with **bootcdwrite**
to disk.

The bootcd can be copied to one or more Disk Partitions seen by the running system.
It is possible to let **bootcd2disk** automatically find a disk, make partitions on it,
copy the cd to the disk and make the disk bootable.

bootcdflopcp will copy changes made in ram to the floppy disk.
**bootcdflopcp** will be available as soon as your system is running from cd.
The floppy has to have a filesystem already. (See mke2fs or mformat).
If you have to boot from floppy, because your cd-drive or bios does
not support to boot from cd a msdos filesystem is used to run syslinux.
**bootcdflopcp** searches for differences between RAM and CD.
For each different file, it checks if it is listed in the files ignore, remove
or change on floppy. If it is listed in change it will be saved to change.tgz
on floppy. If it is listed in remove the file will be removed from ram next
boot time. If it is listed in ignore it will be ignored. If it is not listed
at all you will be interactively asked what to do.

OPTIONS
=======

**-v**

  be verbose

**-d** *device*

  Use *device* instead of **/dev/fd0** to save changes.

FILES
=====

**FLOPPY:/remove**

  Each line in the file **remove** on the floppy disk is a file name.
  Files listed will be deleted from ram next boot time.

**FLOPPY:/ignore**

  Each line in the file **ignore** on the floppy disk is a file name.
  Files listes will be ignored by bootcdflopcp.

**FLOPPY:/change.tgz**

  Files in **change.tgz** on the floppy disk will be copied to ram next
  boot time.

SEE ALSO
========

bootcd(7), bootcdwrite(1), bootcd2disk(1), bootcdmk2diskconf(1),
bootcdwrite.conf(5), bootcd2disk.conf(5)
