===================
 bootcdmk2diskconf
===================

----------------------------------------
build config file usable by bootcd2disk
----------------------------------------

:Author: bernd.schumacher@hpe.com
:Date: 2020-08-07
:Copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
:License: GNU General Public License, version 3
:Version: 0.1
:Manual section: 1
:Manual group: bootcd utils

SYNOPSIS
========
**bootcdmk2diskconf** [**-i|-s|-m**] [**-d** *debug_runtime_config*] -- [**-h|--help**]
  [**--initrd** *INITRD*] [**--kernel** *KERNEL*] [**--samesize**] [**--testdata**]
  [**--exactdisks**] [**--only_vars** *only_vars*] [**--no_parttrans**]

DESCRIPTION
===========

**bootcdmk2diskconf** builds a config file, that can be
used by **bootcd2disk** as **bootcd2disk.conf** config file
and writes it to stdout.
The config file tries to represent the configuration of the
running system

OPTIONS
=======

**-i|-s|-m**

  Run bootcdwrite in interactive, silent or minimal control mode.
  See shellia(1) for this standard **shellia** options.

**-d** *debug_runtime_config*

  Run bootcdwrite in debug mode.
  See shellia(1) for this standard **shellia** options.

**--samesize**

  Use same sizes for disk.
  If this option is not used the last partition gets the rest of the space.

**--testdata**

  use testdata instead of exporing actual system.

**--exactdisks**

  with this optin disks will be defined as found in the actual system.
  Without this option, disks will be defined as **auto**, to be
  calculated on the target system by **bootcd2disk**.

**--initrd** *INITRD*

  use initrd other than
  Default::

    INITRD=initrd.img

**--kernel** *KERNEL*

  use kernel other than
  Default::

    KERNEL=vmlinuz

**-h|--help**

  print this help

**--only_vars** *only_vars*

  only print the variables listed comma separated in *only_vars*

**--no_parttrans**

  Normally a partition name will be transformed to a more generic name.
  Because, on another system the discs may have different names.
  With this options no transformation is done.

**--SRCDISK** *SRCDISK*

  Get Information not from running system, but from system mounted at
  directory *SRCDISK*
  Default::

    SRCDISK="/"

SEE ALSO
========

bootcd(7), bootcdwrite(1), bootcd2disk(1), bootcdflopcp(1),
bootcdwrite.conf(5), bootcd2disk.conf(5)
