# vim: set filetype=sh :
#        file: /usr/share/bootcd/bootcd2disk-check.lib
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2001-2020)
#     license: GNU General Public License, version 3
# description: bootcd2disk-check.lib - functions needed by bootcd2disk

is_disk_used()
{
  cat /proc/mounts /proc/swaps | gawk '{print $1}' | grep "$1" >/dev/null
}
#for i in /dev/vda /dev/xxx; do
#  if is_disk_used $i; then echo "$i is used"; else echo "$i is not used"; fi
#done
#exit 0

# checkfs ext2|ext3|ext4|xfs
checkfs()
{
  local KCONF KERN_YES

  KERN_YES="y"
  [ "$INITRD" ] && KERN_YES="[m|y]" # if initrd is used, a kernel-module is enough

  KCONF="$(cd /; realpath "$KERNEL" | sed "s/vmlinu./config/")"
  if [ ! -f "$KCONF" ]; then
    warn "No Config for KERNEL=<$KERNEL> found. (tried KCONF=<$KCONF>)"
    KCONF=""
  fi

  if [ "$1" = "ext4" ]; then
    [ ! -f /sbin/mkfs.ext4 ] && echo "
/sbin/mkfs.ext4 not found. To use ext4 a new version of e2fsprogs has to be
installed."

    if [ "$KCONF" ]; then
      for i in "CONFIG_EXT4_FS=$KERN_YES"; do
        [ "$(grep "^$i" $KCONF)" ] || echo "
$i is not configured. To use EXT4 some kernel
options are needed. It seems not all needed Options are build in the kernel."
      done
    fi
  elif [ "$1" = "ext3" ]; then
    [ ! -f /sbin/mkfs.ext3 ] && echo "
/sbin/mkfs.ext3 not found. To use EXT3 a new version of e2fsprogs has to be
installed."

    if [ "$KCONF" ]; then
      for i in "CONFIG_EXT3_FS=$KERN_YES"; do
        [ "$(grep "^$i" $KCONF)" ] || echo "
$i is not configured. To use EXT3 some kernel options are needed.
It seems not all needed Options are build in the kernel."
      done
    fi
  elif [ "$1" = "ext2" ]; then
    [ -f /sbin/mkfs.ext2 ] || echo "
/sbin/mkfs.ext2 not found. To use EXT2 a new version of e2fsprogs has to be
installed."
    if [ "$KCONF" ]; then
      if [ ! "$(grep "^CONFIG_EXT4_USE_FOR_EXT2=y" $KCONF)" ]; then
        for i in "CONFIG_EXT2_FS=$KERN_YES"; do
          [ "$(grep "^$i" $KCONF)" ] || echo "
$i is not configured. To use EXT2 some kernel options are needed. It seems
not all needed Options are build in the kernel."
        done
      fi
    fi

  elif [ "$1" = "xfs" ]; then
    [ -f /sbin/mkfs.xfs ] || echo "
/sbin/mkfs.xfs not found. To use XFS a new version of xfsprogs has to be
installed."
  else
    echo "filesystem <$1> unknown."
  fi
}

# replace parameter DISK to the real disk-device
# disk2real [-UUID|-DEVNAME] <var>
disk2real()
{
  local d h1 h2 p u

  u=""
  if [ "$1" = "-UUID" ]; then
    u="UUID=UUID!"
    shift
  elif [ "$1" = "-DEVNAME" ]; then
    shift
  fi

  h1="$1"
  for d in $DISKVARS; do

    # /dev/hda has partitions /dev/hda1 /dev/hda2 ... => p=""
    # /dev/sda has partitions /dev/sda1 /dev/sda2 ... => p=""
    # /dev/ida/c0d0 has partitions /dev/ida/c0d0p1 /dev/ida/c0d0p2 ... => p="p"
    if [ -b "$(eval "echo \"\${DISK$d}p1\"")" ]; then
      # We really found a device ending with p1
      p="p"
    elif [ -b "$(eval "echo \"\${DISK$d}1\"")" ]; then
      # We really found a device ending with <not-p>1
      p=""
    elif [ "$(eval "echo \"\${DISK$d}\"" | grep "[[:digit:]]$")" ]; then
      # because of udev not all devicefilese may be present
      # We guess the devicename ends with p1
      p="p"
    else
      # We guess the devicename ends not with p1
      p=""
    fi

    h2="$h1"
    #h1="$(eval "echo \"$(echo "$h2" | ia_easy_backslash)\" | sed -E -e \"s|\<DISK$d\>|$u\$DISK$d|g\" -e \"s|\<DISK${d}P|$u\${DISK$d}${p}|g\"")"
    h1="$(eval "echo \"$(echo "$h2" | ia_easy_backslash)\" | sed -E \
      -e \"s|$u\<DISK$d\>|$u\$DISK$d|g\" \
      -e \"s|$u\<DISK${d}P|$u\${DISK$d}${p}|g\" \
      -e \"s|\<DISK$d\>|$u\$DISK$d|g\" \
      -e \"s|\<DISK${d}P|$u\${DISK$d}${p}|g\"")"
  done
  echo "$h1"
}
#lib_dev_test() {
#  DISK0="/dev/hda"; DISKVARS=0
#  RES="$(disk2real "a DISK0 b DISK0P1 c DISK0P2 d \$badvar")"
#  EXP="a /dev/hda b /dev/hda1 c /dev/hda2 d \$badvar"
#  [ "$RES" = "$EXP" ] && echo "OK disk2real-1" || echo "ERR disk2real-1 EXP=<$EXP> RES=<$RES>"
#  DISK0="/dev/cciss/p0d0"; DISKVARS=0
#  RES="$(disk2real "a DISK0 b DISK0P1 c DISK0P2 d \$badvar")"
#  EXP="a /dev/cciss/p0d0 b /dev/cciss/p0d0p1 c /dev/cciss/p0d0p2 d \$badvar"
#  [ "$RES" = "$EXP" ] && echo "OK disk2real-2" || echo "ERR disk2real-2 EXP=<$EXP> RES=<$RES>"
#  DISK0="/dev/hda"; DISK1="/dev/hdb"; DISK2="/dev/hdc"; DISKVARS="0 1 2"
#  RES="$(disk2real "a DISK0 b DISK1P1 c DISK2P2 d \$badvar")"
#  EXP="a /dev/hda b /dev/hdb1 c /dev/hdc2 d \$badvar"
#  [ "$RES" = "$EXP" ] && echo "OK disk2real-3" || echo "ERR disk2real-3 EXP=<$EXP> RES=<$RES>"
#  DISK0="/dev/cciss/p0d0"; DISK1="/dev/cciss/p1d2"; DISK2="/dev/hdc"; DISKVARS="0 1 2"
#  RES="$(disk2real "a DISK0 b DISK1P1 c DISK2P2 d \$badvar")"
#  EXP="a /dev/cciss/p0d0 b /dev/cciss/p1d2p1 c /dev/hdc2 d \$badvar"
#  [ "$RES" = "$EXP" ] && echo "OK disk2real-4" || echo "ERR disk2real-4 EXP=<$EXP> RES=<$RES>"
#  DISK0="/dev/hda"; DISKVARS="0"
#  RES="$(disk2real -UUID "a DISK0 b UUID=UUID!DISK0P1")"
#  EXP="a UUID=UUID!/dev/hda b UUID=UUID!/dev/hda1"
#  [ "$RES" = "$EXP" ] && echo "OK disk2real-5" || echo "ERR disk2real-5 EXP=<$EXP> RES=<$RES>"
#}

# disk2realvar [-UUID|-DEVNAME] <var>
disk2realvar()
{
  local opt

  opt=""
  if [ "$(echo " -UUID -DEVNAME " | grep " $1 ")" ]; then
    opt="$1"
    shift
  fi

  eval "$1=\"\$(disk2real $opt \"\$$1\")\""
}
#lib_dev_test() {
#  DISK0="/dev/hda"; DISKVARS="0"
#  RES="a DISK0 b DISK0P1 c DISK0P2 d \$badvar"; disk2realvar RES
#  EXP="a /dev/hda b /dev/hda1 c /dev/hda2 d \$badvar"
#  [ "$RES" = "$EXP" ] && echo "OK disk2realvar-1" || echo "ERR disk2realvar-1 EXP=<$EXP> RES=<$RES>"
#  DISK0="/dev/cciss/p0d0"; DISKVARS="0"
#  RES="a DISK0 b DISK0P1 c DISK0P2 d \$badvar"; disk2realvar RES
#  EXP="a /dev/cciss/p0d0 b /dev/cciss/p0d0p1 c /dev/cciss/p0d0p2 d \$badvar"
#  [ "$RES" = "$EXP" ] && echo "OK disk2realvar-2" || echo "ERR disk2realvar-2 EXP=<$EXP> RES=<$RES>"
#  DISK0="/dev/hda"; DISK1="/dev/hdb"; DISK2="/dev/hdc"; DISKVARS="0 1 2"
#  RES="a DISK0 b DISK1P1 c DISK2P2 d \$badvar"; disk2realvar RES
#  EXP="a /dev/hda b /dev/hdb1 c /dev/hdc2 d \$badvar"
#  [ "$RES" = "$EXP" ] && echo "OK disk2realvar-3" || echo "ERR disk2realvar-3 EXP=<$EXP> RES=<$RES>"
#  DISK0="/dev/cciss/p0d0"; DISK1="/dev/cciss/p1d2"; DISK2="/dev/hdc"; DISKVARS="0 1 2"
#  RES="a DISK0 b DISK1P1 c DISK2P2 d \$badvar"; disk2realvar RES
#  EXP="a /dev/cciss/p0d0 b /dev/cciss/p1d2p1 c /dev/hdc2 d \$badvar"
#  [ "$RES" = "$EXP" ] && echo "OK disk2realvar-4" || echo "ERR disk2realvar-4 EXP=<$EXP> RES=<$RES>"
#}

# real2uuid
# Change real devices defined as UUID!<real device> to
# <uuid of real device>
real2uuid()
{
  local h1 realdevs realdev uuid
  h1="$1"

  realdevs="$(echo "$h1" | sed -n "s/.*UUID!\([^:[:space:]'\"]\+\).*/\1/p")"

  for realdev in $realdevs ;do
    uuid="$(blkid -o value -s UUID "$realdev" | head -1)"
    if [ ! "$uuid" ]; then
      err "can not get uuid for <$realdev> with blkid"
    fi
    h1="$(echo "$h1" | sed "s|UUID!$realdev\>|$uuid|g")"
  done

  echo "$h1"
}

# avail_from_allow_disks "<available_disks>" "<allowed_disks>"
#   order from allowed_disks will be respected
avail_from_allow_disks()
{
  local av al re                            # exp:
  av="$(echo "$1" | tr '\n' ' ')"           # av="/dev/a /dev/b"
  al="$(echo "$2" | tr '\n' ' ')"           # al="/dev/disk/by<link-to-/dev/b> /dev/c"
  al="$(echo "$al" | xargs -r realpath -m | tr "\n" " ")" # al="/dev/b /dev/c"
  re="$(echo "$av" | sed -E -e "s/ +/|/g")" # re="/dev/a|/dev/b"
  re="s:^($re)$:\1:p"                       # re="s:^(/dev/a|/dev/b)$:<>:p"
  echo "$al" | sed -E -e "s/ +/\n/g" | sed -n -E -e "$re" |
    tr "\n" " " | sed "s/ $//"              # /dev/b
}
#fkt="avail_from_allow_disks"
#a="/dev/a /dev/b"; b="/dev/b /dev/c"; exp="/dev/b"
#res="$(avail_from_allow_disks "$a" "$b")"
#[ "$res" = "$exp" ] && echo "OK 1 $f" || echo "ERR 1 $f res=<$res> exp=<$exp>"
#a="/dev/a
#/dev/b"; b="/dev/b
#/dev/c"; exp="/dev/b"
#res="$(avail_from_allow_disks "$a" "$b")"
#[ "$res" = "$exp" ] && echo "OK 2 $f" || echo "ERR 2 $f res=<$res> exp=<$exp>"
#a="/dev/a /dev/b /dev/c"; b="/dev/d /dev/c /dev/b"; exp="/dev/c /dev/b"
#res="$(avail_from_allow_disks "$a" "$b")"
#[ "$res" = "$exp" ] && echo "OK 3 $f" || echo "ERR 3 $f res=<$res> exp=<$exp>"
#a="/dev/c /dev/b /dev/a"; b="/dev/b /dev/c /dev/d"; exp="/dev/b /dev/c"
#res="$(avail_from_allow_disks "$a" "$b")"
#[ "$res" = "$exp" ] && echo "OK 4 $f" || echo "ERR 4 $f res=<$res> exp=<$exp>"
#a="/dev/notexist/disk"; b="/dev/notexist/disk"; exp="/dev/notexist/disk"
#res="$(avail_from_allow_disks "$a" "$b")"
#[ "$res" = "$exp" ] && echo "OK 5 $f" || echo "ERR 5 $f res=<$res> exp=<$exp>"
#exit

### check_ functions in alphabetical order

check_BOOTCDMP()
{
  if [ ! "$BOOTCDMP" ]; then
    err "BOOTCDMP=<$BOOTCDMP> must not be empty"
  elif [ ! "$(echo "$BOOTCDMP" | grep "^/")" ]; then
    err "BOOTCDMP=<$BOOTCDMP> must start with /"
  fi
}

check_COPYDEST()
{
  if [ ! "$COPYDEST" ]; then
    err "COPYDEST=<$COPYDEST> must not be empty"
  elif [ ! "$(echo "$COPYDEST" | grep "^/")" ]; then
    err "COPYDEST=<$COPYDEST> must start with /"
  fi
}

check_DISABLE_CRON()
{
  :
}

# add_komma_and [-n <name>] <space separated list>
add_komma_and()
{
  n=""
  if [ "$1" = "-n" ]; then
    n="$2"
    shift 2
  fi
  echo "$1" | sed -E -e "s/\s*$//" -e "s/(\S+)/${n}\1/g" -e "s/ /, /g" -e "s/(.+), (\S+)/\1 and \2/"
}
#f="add_komma_and"
#res="$(add_komma_and one)"; exp="one"
#[ "$res" = "$exp" ] && echo "OK $f 1" || echo "ERR $f 1 res=<$res> exp=<$exp>"
#res="$(add_komma_and -n disk "1 2 3")"; exp="disk1, disk2 and disk3"
#[ "$res" = "$exp" ] && echo "OK $f 2" || echo "ERR $f 2 res=<$res> exp=<$exp>"
#res="$(add_komma_and -n disk "   ")"; exp=""
#[ "$res" = "$exp" ] && echo "OK $f 3" || echo "ERR $f 3 res=<$res> exp=<$exp>"
#exit

# check_DISK
# INPUT: DISK#
# OUTPUT: DISKVARS, DISK# (no auto),
check_DISK()
{
  local v i re_DISK_SFDISKOPTS h auto noauto AUTODISK msg

  DISKVARS="$(set | sed -n "s/^DISK\([[:digit:]]\+\)=.*/\1/p" | tr "\n" " " | sed "s/ $//")"
  dbglog "check_DISK: DISKVARS=<$DISKVARS>"
  msg="Needed disks are $(add_komma_and -n disk "$DISKVARS")."

  [ "$DISKVARS" ] || err "check_DISK: no DISK# is defined."

  # DISK could be defined as "<device> <SFDISKOPTS>"
  # This is experimental and undocumented.
  # re_DISK_SFDISKOPTS is the regex to split this
  re_DISK_SFDISKOPTS="\([^[:space:]]\+\)[[:space:]]*\(.*\)"

  auto=""
  noauto=""
  # exp input: DISKVARS="1 3 4"
  # exp input: DISK1=auto DISK3=/dev/md0 DISK4=auto
  for i in $DISKVARS; do
    eval "t=\"$(eval "echo \"\$DISK$i\"" | sed "s/$re_DISK_SFDISKOPTS/\1/")\""
    if [ "$t" = "auto" ]; then
      [ "$auto" ] && auto="$auto $i" || auto="$i"
    else
      [ "$noauto" ] && noauto="$noauto $i" || noauto="$i"
    fi
  done
  # example output: auto="1 4", noauto="3"
  if [ "$DISKVARS" != "$auto" ]; then
    msg="$msg Needed disks to be defined are $(add_komma_and -n disk "$auto")."
  fi

  # lsblk: --exclude 2,11 : exlcude FLOPPY disks(2), 11=SCSI CD-ROM devices(11)
  # exp: system with 2 disks: AUTODISK="/dev/sda /dev/sdb"
  if [ "$(lsblk --help | egrep "^\s+PATH\s")" ]; then
    # lsblk 2.33.1 has option "--output PATH"
    AUTODISK="$(lsblk --noheadings --exclude 2,11 --nodeps --output PATH | tr '\n' ' ')"
  else
    # lsblk 2.29.2 / util-linux 2.29.2-1+deb9u1 / debian 9.13 has no no option "--output PATH"
    AUTODISK="$(lsblk --noheadings --exclude 2,11 --nodeps --output NAME | sed "s/^/\/dev\//" | tr '\n' ' ')"
  fi

  dbglog "check_DISK: AUTODISK=<$AUTODISK> # all disks found on system"
  msg="$msg Found $(add_komma_and "$AUTODISK") on system."

  h="$AUTODISK"
  if [ "$ALLOWEDDISKS" ]; then
    AUTODISK="$(avail_from_allow_disks "$AUTODISK" "$ALLOWEDDISKS")"
  fi
  dbglog "check_DISK: AUTODISK=<$AUTODISK> (only ALLOWEDDISKS)"
  if [ "$h" != "$AUTODISK" ]; then
    if [ "$AUTODISK" ]; then
      msg="$msg Found and not excluded by ALLOWEDDISKS are $(add_komma_and -n disk "$AUTODISK")."
    else
      msg="$msg All found disks excluded by ALLOWEDDISKS."
    fi
  fi

  if [ ! "$only_unmount" ]; then
    h="$AUTODISK"
    for i in $AUTODISK; do
      if is_disk_used $i; then
        AUTODISK="$(echo " $AUTODISK "| sed -e "s| $i | |" -e "s|^ ||" -e "s| $||")"
      fi
    done
    dbglog "check_DISK: AUTODISK=<$AUTODISK> (without disks used for mount or swap)"
    if [ "$h" != "$AUTODISK" ]; then
      if [ "$AUTODISK" ]; then
        msg="$msg Only $(add_komma_and -n disk "$AUTODISK") are not already used."
      else
        msg="$msg No found disk is not already used."
      fi
    fi
  fi

  h="$AUTODISK"
  for i in $noauto; do
    eval "v=\"\$DISK$i\""
    AUTODISK="$(echo " $AUTODISK "| sed -e "s| $i | |" -e "s|^ ||" -e "s| $||")"
  done
  # exp: AUTODISK="/dev/sda /dev/sdb"
  dbglog "check_DISK: AUTODISK=<$AUTODISK> # without manual assigned disk"
  if [ "$h" != "$AUTODISK" ]; then
    msg="$msg Only $(add_komma_and -n disk "$AUTODISK") are not assigned manually."
  fi

  n=0
  for i in $auto; do
    n=$(($n+1))
    eval "v=\"$(echo "$AUTODISK" |gawk "{print \$$n}")\""
    if [ ! "$v" ]; then
      RETURN_ERR="check_DISK: $msg Could not automatically calculate DISK$i."
      return 5
    fi
    eval "DISK$i=\"$v\""
    eval "SFDISKOPTS$i=\"\""
    eval "dbglog \"check_DISK: DISK$i=<\$DISK$i> SFDISKOPTS$i=<\$SFDISKOPTS$i>\""
    msg="$msg DISK$i=$v"
  done

  for i in $noauto;  do
    eval "DISK$i=\"$(eval "echo \"\$DISK$i\"" | sed "s/$re_DISK_SFDISKOPTS/\1/")\""
    eval "SFDISKOPTS$i=\"$(eval "echo \"\$DISK$i\"" | sed "s/$re_DISK_SFDISKOPTS/\2/")\""
    eval "dbglog \"check_DISK: DISK$i=<\$DISK$i> SFDISKOPTS$i=<\$SFDISKOPTS$i>\""
  done
}
#lib_dev_test() {
#  lsblk() { echo "/dev/sda1 /dev/sda2 /dev/sda3"; }
#  unset  $(seq 0 99 | sed "s/^/DISK/"); DISK0="auto"; DISK3="auto" DISK7="auto";
#  check_DISK
#  RES="<$DISKVARS> $DISK0 $DISK3 $DISK7"; EXP="<0 3 7> /dev/sda1 /dev/sda2 /dev/sda3"
#  [ "$RES" = "$EXP" ] && echo "OK check_DISK-1" || echo "ERR check_DISK-1 EXP=<$EXP> RES=<$RES>"
#  lsblk() { echo "/dev/sda1 /dev/sda2 /dev/sda3"; }
#  unset  $(seq 0 99 | sed "s/^/DISK/"); DISK0="auto"; DISK3="/dev/md0" DISK7="auto";
#  check_DISK
#  RES="<$DISKVARS> $DISK0 $DISK3 $DISK7"; EXP="<0 3 7> /dev/sda1 /dev/md0 /dev/sda2"
#  [ "$RES" = "$EXP" ] && echo "OK check_DISK-2" || echo "ERR check_DISK-2 EXP=<$EXP> RES=<$RES>"
#}

check_DISKIDTYP()
{
  dbglog "check_DISKIDTYP: start: DISKIDTYP=<$DISKIDTYP>"
  if [ "$DISKIDTYP" = "UUID" -o "$DISKIDTYP" = "DEVNAME" ]; then
    :
  else
    err "Unknown DISKIDTYP: $DISKIDTYP defined."
  fi
  dbglog "check_DISKIDTYP: DISKIDTYP=<$DISKIDTYP>"
}

check_DISKpartition()
{
  eval "$1=\"$1\""
  disk2realvar "$1"
}

check_EFIBOOT()
{
  local f1 p1
  p1=grub-pc-bin
  f1=/usr/lib/grub/i386-pc/modinfo.sh
  if [ "$EFIBOOT" = "auto" ] ; then
    [ -d /sys/firmware/efi ] && EFIBOOT="efi" || EFIBOOT="bios"
  elif [ "$EFIBOOT" != "efi" ] && [ "$EFIBOOT" != "bios" ] && [ "$EFIBOOT" != "bios+efi" ] ; then
    err "EFIBOOT=<$EFIBOOT> must be efi, bios, bios+efi or auto"
  fi

  if [ "$EFIBOOT" = "bios" -o "$EFIBOOT" = "bios+efi" ]; then
    [ -f $f1 ] || err "EFIBOOT=<$EFIBOOT> but $f1 is not installed. Please install $p1"
  fi
}

check_EXT2FS()
{
  if [ "$EXT2FS" ]; then
    e="$(checkfs ext2)"
    if [ "$e" ]; then
      err "EXT2FS=<$EXT2FS> is defined but $e"
    fi
    EXT2FS=$(disk2real "$EXT2FS")
  fi
}

check_EXT3FS()
{
  if [ "$EXT3FS" ]; then
    e="$(checkfs ext3)"
    if [ "$e" ]; then
      err "EXT3FS=<$EXT3FS> is defined but $e"
    fi
    EXT3FS=$(disk2real "$EXT3FS")
  fi
}

check_EXT4FS()
{
  if [ "$EXT4FS" ]; then
    e="$(checkfs ext4)"
    if [ "$e" ]; then
      err "EXT4FS=<$EXT4FS> is defined but $e"
    fi
    EXT4FS=$(disk2real "$EXT4FS")
  fi
}

check_EXCLUDE()
{
  # EXCLUDE must start with / or "/

  eval "set -- $EXCLUDE"
  for i in "$@"; do
    [ "$(echo "$i" | grep -E -e "^/" -e "^\"/")" ] ||
      err "EXCLUDE=<$EXCLUDE> contains <$i> that does not start with </>"
  done
}
#err() { echo "$*"; }
#f="check_EXCLUDE"
#EXCLUDE=""; res="$($f)"; exp=""
#[ "$res" = "$exp" ] && echo "OK $f-1" || echo "ERR $f-1 res=<$res> exp=<$exp>"
#EXCLUDE="/one/two /three/four /five/six"; res="$($f)"; exp=""
#[ "$res" = "$exp" ] && echo "OK $f-2" || echo "ERR $f-2 res=<$res> exp=<$exp>"
#EXCLUDE="/one/two three/four /five/six"; res="$($f)"
#exp="EXCLUDE=</one/two three/four /five/six> contains <three/four> that does not start with </>"
#[ "$res" = "$exp" ] && echo "OK $f-3" || echo "ERR $f-3 res=<$res> exp=<$exp>"
#EXCLUDE="/one/two \"three/*\" /five/six"; res="$($f)"
#exp="EXCLUDE=</one/two \"three/*\" /five/six> contains <three/*> that does not start with </>"
#[ "$res" = "$exp" ] && echo "OK $f-4" || echo "ERR $f-4 res=<$res> exp=<$exp>"
#EXCLUDE="/one/two /three/four/ /five/six"; res="$($f)"
#exit 0

check_EXCLUDE_SYSTEM()
{
  # EXCLUDE_SYSTEM must start with / or "/

  for i in $EXCLUDE_SYSTEM; do
    [ "$(echo "$i" | grep -E -e "^/" -e "^\"/")" ] ||
      err "EXCLUDE=<$EXCLUDE_SYSTEM> contains <$i> that does not start with </>"
  done
}

check_FSTAB()
{
  dbglog 9 check "check_FSTAB: start: FSTAB=<$FSTAB> DISKIDTYP=<$DISKIDTYP>"
  FSTAB="$(disk2real "-$DISKIDTYP" "$FSTAB")"
  dbglog 8 check "check_FSTAB: end: FSTAB=<$FSTAB>"
}

check_GRUB()
{
  GRUB="$(disk2real "$GRUB")"
}

check_GRUB_INSTALL_OPTS_BIOS()
{
  :
}

check_GRUB_INSTALL_OPTS_EFI()
{
  :
}

check_GRUB2()
{
  GRUB2="$(disk2real "$GRUB2")"
}

check_GRUBBOOTDISK()
{
  :
}

check_GRUBBOOTDIR()
{
  :
}

check_GRUBDEVICEMAP()
{
  :
}

check_IMAGEURL()
{
  if [ "$IMAGEURL" ]; then
    [ "$SWAP" ] || err "If you define IMAGEURL or \"-url\" a swap partition must be defined!"
    log "Please verify the size of the swap partition. It must be greater then the image!"
  fi
}

check_INITRD()
{
  INITRD="$(cd /; realpath $INITRD)"
  [ $? -eq 0 ] || err "Wrong Definition of INITRD=<$INITRD>"
}

check_ISOLOOPBACK()
{
  local I
  # if not empty must be start with /
  if [ "$(echo "$ISOLOOPBACK" | grep -E -e "^\s*[^/]\S")" ]; then
    err "ISOLOOPBACK=<$ISOLOOPBACK> must start with /"
  fi

  # must not start or end with spaces
  # must not contain more than one / on one place
  # must not end with /
  I="$(echo "$ISOLOOPBACK" | sed -E -e "s/^\s+//" -e "s/\s+$//" -e "s|/+|/|g" -e "s|/$||")"
  if [ "$I" != "$ISOLOOPBACK" ]; then
    dbglog 8 check "check_ISOLOOPBACK changed from <$ISOLOOPBACK> to <$I>"
    ISOLOOPBACK="$I"
  fi
}

check_KERNEL()
{
  eval "set -- $KERNEL"
  [ $# -eq 1 ] || err "Wrong Definition of KERNEL ($# KERNELs instead of 1 KERNEL)"
  KERNEL="$(cd /; realpath "$1")"
  [ $? -eq 0 ] || err "Wrong Definition of KERNEL=<$KERNEL> not found"
}

check_LILO()
{
  LILO="$(disk2real "$LILO")"
}

check_LINUXFS()
{
  local i err
  if [ "$LINUXFS" = "auto" ]; then
    for i in ext4 ext3 ext2 xfs; do
      if [ ! "$(checkfs $i)" ]; then
        LINUXFS="$i"
        break
      fi
    done
  else
    err="$(checkfs "$LINUXFS")"
    [ ! "$err" ] || err "$err"
  fi
}

check_LUKS()
{
  disk2realvar "LUKS"
}

check_LVMGRP()
{
  LVMGRP="$(disk2real "$LVMGRP")"
}

check_LVMVOL()
{
  LVMVOL="$(disk2real "$LVMVOL")"
}

check_MD()
{
  disk2realvar "MD"
}

check_ORDER()
{
  if [ ! "$ORDER" ]; then
    err "ORDER=<$ORDER> must not be empty"
  else
    for i in $ORDER; do
      [ "$(echo " parti luks md lvm " | grep " $i ")" ] || err "ORDER=<$ORDER> contains <$i>, but only parti|md|lvm allowed"
    done
    for i in $(echo "$ORDER" | tr " " "\n" | sort | uniq -d); do
      [ "$(echo " parti " | grep " $i ")" ] || err "ORDER=<$ORDER> contains multible <$i>, but only parti may be multible"
    done
  fi
}

check_PARTITIONLABEL()
{
  disk2realvar "PARTITIONLABEL"
}

check_RESUME()
{
  disk2realvar "-$DISKIDTYP" "RESUME"
  dbglog 8 check "check_RESUME RESUME=<$RESUME>"
}

check_SFDISK()
{
  local SFDISKVARS i

  SFDISKVARS="$(set | sed -n "s/^SFDISK\([[:digit:]]\+\)=.*/\1/p" | tr "\n" " " | sed "s/ $//")"
  dbglog "check_SFDISK: SFDISKVARS=<$SFDISKVARS>"

  for i in $SFDISKVARS; do
    [ "$(echo " $DISKVARS " | grep " $i ")" ] || err "SFDISK$i is defined but no DISK$i exists"
  done

  for i in $SFDISKVARS; do
    eval "dbg 6 check \"check_SFDISK SFDISK$i=<\$SFDISK$i>\""
    disk2realvar "SFDISK$i"
    eval "dbg 6 check \"check_SFDISK SFDISK$i=<\$SFDISK$i>\""
  done
}

check_SSHHOSTKEY()
{
  if [ ! "$SSHHOSTKEY" ]; then
    err "SSHHOSTKEY=<$SSHHOSTKEY> must not be empty"
  else
    [ "$(echo " yes no " | grep " $SSHHOSTKEY ")" ] ||
      err "SSHHOSTKEY=<$SSHHOSTKEY> must be yes|no"
  fi
}

check_SWAP()
{
  dbglog "check_SWAP: input: SWAP=<$SWAP>"
  disk2realvar "SWAP"
  dbglog "check_SWAP: output: SWAP=<$SWAP>"
}

check_ALLOWEDDISKS()
{
  :
}

check_UDEV_FIXNET()
{
  :
  #TODO
}

check_VFAT()
{
  VFAT=$(disk2real "$VFAT")
}

check_XFS()
{
  XFS=$(disk2real "$XFS")
}

bootcd_global()
{
  local i
  dbglog 2 check "bootcd_global: begin <$*>"
  for i in $*; do
    dbglog 2 check "bootcd_global: checking <$i>"
    if [ "$(echo "$i" | grep -E -e "^DISK([0-9]|[1-9][0-9])P[1-9]([0-9]|)$")" ]; then
      check_DISKpartition "$i"
    else
      err "bootcd_global: No way to check $i"
    fi
    eval "dbglog 1 check \"bootcd_global: $i=<\$$i>\""
  done
}
