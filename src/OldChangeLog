2007-05-05  Bernd Schumacher <bernd.schumacher@hp.com>
	* Resolved Bug "depends on non-essential package initramfs-tools in
	postrm"
	* Resolved Bug "/var is not writable", when booting from bootcd with
	udev, because udev was not restarting after using ramdisk for /dev

2007-04-27  Bernd Schumacher <bernd.schumacher@hp.com>
	* if grub-install does not work in bootcd2disk grub will be used
	as before. This is because, on one of my machines grub-install says:
	"/dev/cciss/c0d0 does not have any corresponging BIOS drive.". But
	grub works.

2007-04-17  Bernd Schumacher <bernd.schumacher@hp.com>
	* Added new option -d to bootcdflopcp as sggested in a similar
	way by Pablo Pessolani [ppessolani@hotmail.com].
	* bootcd2disk now only uses pvremove if pv are found
	* patches in manpages and FAQ
	* bootcdflopcp has new option -d

2007-04-15  Bernd Schumacher <bernd.schumacher@hp.com>
	* Changed bootcd2disk to be able to handle multiple harddisks.
	This is needed for LVM installation.

2007-04-10  Carsten Dinkelmann <din@foobar-cpa.de>
	* Added a new argument -url to bootcd2disk for fast Installing

2007-03-27  Bernd Schumacher <bernd.schumacher@hp.com>
	* Added before_copy() function
	* added /tmp/isoloopback feature (see FAQ)
	* Started new Version 3.02

2007-02-19  Bernd Schumacher <bernd.schumacher@hp.com>
	* Started new Version 3.01
	* Updated copyright. I hate DRM but I use GPL v2 because of freedom.
	* Copy module sr_mod to initrd if available (needed for module cdrom)
	* Moved config from /etc/initrfamfs-tools to /usr/share/initramfs-tools

2006-12-08  Bernd Schumacher <bernd.schumacher@hp.com>
	* Added support for LVM
	* removed bootcd-mkinitrd

2006-12-01  Bernd Schumacher <bernd.schumacher@hp.com>
	* bootcdwrite gives more verbose hints if cd or ram is to small
	* Added patch from Volker Wysk <post@volker-wysk.de> to fix xargs
	unmatched single quote

2006-11-21  Bernd Schumacher <bernd.schumacher@hp.com>
	* tested udev system with bootcdwrite and bootcd2disk
	* added bootcd-mkinitramfs
	* S12bootcdram.sh now uses udev stop + start instead of only start
	* Started new version 2.55

2006-11-04  Bernd Schumacher <bernd.schumacher@hp.com>
	* use realpath with option -s for files in NOT_TO_CD and NOT_TO_RAM

2006-10-28  Bernd Schumacher <bernd.schumacher@hp.com>
	* find grub as /usr/sbin/grub and /sbin/grub

2006-06-23  Bernd Schumacher <bernd.schumacher@hp.com>
	* Started new version 2.54
	* Fixed Rock Ridge filename collision problem reported by Gabor Burjan
	<buga@buvoshetes.hu>.
	* grub may be installed as /sbin/grub or /usr/sbin/grub

2006-04-12  Bernd Schumacher <bernd.schumacher@hp.com>
	* Started new version 2.53
	* Fixed typos reported by A Costa <agcosta@gis.net>
	* Minimal support for compressed cpio initrd

2006-02-23  Bernd Schumacher <bernd.schumacher@hp.com>
	* Released Version 2.52

2006-02-13  Bernd Schumacher <bernd.schumacher@hp.com>
	* Added udev fix from Uwe Maier [umaier@gmx.de] to boot from
	kernels >2.6.8: "[ -x /etc/init.d/udev ] && /etc/init.d/udev start" to
	the end of S12bootcdram.sh.
	* Changed bootcdmodprobe to try usb-modules in an order used by
	many other people
	* Added fix from from Uwe Maier [umaier@gmx.de] which uses "tail -1" to
	find only one devicefile with given major number and minor number 0.

2006-01-09 frank.hoellering@studio23.info
	* Split boocd2disk: create bootcd2disk.lib to be able 2 use
	functions for after copy operations
	* exclude mountpoints beneath /mnt from cpio

2005-12-24  Bernd Schumacher <bernd.schumacher@hp.com>
	* Released Version 2.51 with only small bugfixes

2005-10-14  Bernd Schumacher <bernd.schumacher@hp.com>
	* Tested new version
	* Released 2.50

2005-10-10  Bernd Schumacher <bernd.schumacher@hp.com>
	* New Version 2.50
	* added grub support

2005-08-05  Bernd Schumacher <bernd.schumacher@hp.com>
	* tested extra_changes()
	* New Version 2.49
	* added missing ChangeLog entries
	* added extra_changes() to bootcdwrite.conf.5 manpage

2005-06-22  Bernd Schumacher <bernd.schumacher@hp.com>
	* There was a bug with the NOT_TO_DISK/NOT_TO_RAM feature.
	Now the full path of files/dirs excluded by bootcd will be checked.
	* It is now possible to change files on the cd before creating the ISO
	with the function extra_changes() in bootcdwrite.conf

2005-06-11  Bernd Schumacher <bernd.schumacher@hp.com>
	* waiting for usb is now done in bootcdproberoot. If the bootdevice is
	found waiting will be stopped, but if necessary waiting will be
	longer as before.

2005-04-21  Bernd Schumacher <bernd.schumacher@hp.com>
	* Changed bootcdmkinitrd, so that it can be recalled without
	problems after upgrading the kernel.
	* Added bootcd-mkinitrd.prerm, to make sure mkinitrd can be
	called without problems after deinstalling bootcd-mkinitrd.
	* Changed bootcdmkinitrd to be able to handle also kernel 2.6
	modules which are named *.ko instead of *.o.

2005-04-19  Bernd Schumacher <bernd.schumacher@hp.com>
	* Changed S13bootcdflop.sh to umount /mnt instead of umount /dev/fd0
	* Changed bootcdtest.sh to be more interactive

2005-04-11  Bernd Schumacher <bernd.schumacher@hp.com>
	* Better handling of bootcd2disk.conf/EXT3=auto option. If running
	with INITRD, it is ok if CONFIG_EXT3_FS is defined as "m" or "y" to
	set EXT3 to yes. This solves the following error after booting from
	a DISK installed with bootcd2disk:
	  ext3: No journal on filesystem on sd(8,3)
	  pivot_root: No such file or directory
	  /sbin/init: 431: cannot open dev/console: No such file
	  Kernel panic: Attempted to kill init!
	* Changed version to 2.48

2005-02-11  Bernd Schumacher <bernd.schumacher@hp.com>
	* Tests and Bugfixes

2005-02-06  Bernd Schumacher <bernd.schumacher@hp.com>
	* Changed possible value DVDPLUS for TYP to DVD
	* removed bootcd-dvdplus package
	* Wrote Mail to orphan bootcd-dvdplus

2005-02-04  Bernd Schumacher <bernd.schumacher@hp.com>
	* Automatic SRCDISK adding to <X> will only happen if <X> is not
	empty and does not start with "/"
	* Added FAQ "Burning the image"

2005-02-03  Carsten Dinkelmann <Carsten.Dinkelmann@foobar-cpa.de>
	* change handling of INITRD, KERNEL, DISABLE_CRON, NOT_TO_CD and
	NOT_TO_RAM. Automaticly add SRCDISK to given paths
	* add proberootdev to automaticly detect the bootcd on SCSI and IDE
	cdroms, if option root="auto" is set
	* add man page for bootcdwrite.conf
	* fix problem with KERNEL="/vmlinuz"
	* remove p, take realpath instead
	* remove bootcd-dvdplus

2005-01-22  Bernd Schumacher <bernd.schumacher@hp.com>
	* Added debian/compat with newest version 4 and
	  changed everything neccessary to build the packages
	* Added function get_border_links

2005-01-21  Bernd Schumacher <bernd.schumacher@hp.com>
	* Changed version to 2.46
	* fixed Improper copyright file
	* fixed bug bootcd-386 is uninstallable on powerpc.
	* fixed bug "MKISOFS_CHNG doesn't work". Verified, that
	  everything patched by Alexey, was already fixed on
	  2005-01-06. Needed to add the adding of /sys.
	* fixed the defaults in bootcdwrite.conf to be a bit more
	  sensible
	* Added entry 2.3 to FAQ

2005-01-12  Bernd Schumacher <bernd.schumacher@hp.com>
	* changed chmod from 777 to 1777 for /ram1/tmp in script
	S12bootcdram.sh

2005-01-06  Bernd Schumacher <bernd.schumacher@hp.com>
	* Changed handling of NOTCOMPRESSED. Now You have to define the Path
	as it is on the CD, (without $SRCDISK and with /var.ro instead of /var)
	* fixed bug in MKISOFS_OPTS. Now added options will be added
	before "arguments without options"

2004-12-17  Bernd Schumacher <bernd.schumacher@hp.com>
	* Tested and fixed option NOTCOMPRESSED
	* Released 2.45

2004-12-03  Bernd Schumacher <bernd.schumacher@hp.com>
	* applied patch from Mark Clarkson <markjclarkson@btinternet.com>
	to fix the bug with DEVFS=yes, and the missing /dev on the boot image
	* Start of new version 2.45
	* added new config option NOTCOMPRESSED

2004-11-16  Bernd Schumacher <bernd.schumacher@hp.com>
	* added "run" to "cp isolinux/syslinux.cfg" to check for errors

2004-10-29  Bernd Schumacher <bernd.schumacher@hp.com>
	* bootcdmkinitrd enhancements to be able to boot from usb cdroms

2004-10-08  Bernd Schumacher <bernd.schumacher@hp.com>
	* Fixed small bug, which could be seen, when calling bootcdmkinitrd
	without option.
	* Separared syslinux.cfg and isolinux.cfg

2004-10-03  Bernd Schumacher <bernd.schumacher@hp.com>
	* Start of new version 2.44.
	* Add new Package bootcd-i386 for better dependencies.
	* Move function do_syslog from bootcdwrite to bootcd-i386.lib.
	* Change values for ARCH to debian standard values.
	* Do not copy /sys, as it is already done for /proc.
	* Added bugreporter names I forgot in version 2.43 in changelog.

2004-08-14  Bernd Schumacher <bernd.schumacher@hp.com>
	* Do not allow to define /mnt in NOT_TO_CD, because
	bootcd2disk trys to mount the destination disk to /mnt and would fail
	* Let people add and delete options given to mkisofs by bootcdwrite.
	* start of new version 2.43

2004-07-22  Bernd Schumacher <bernd.schumacher@hp.com>
	* bootcd2disk works now if /var or a subdir of /var will be a mounted
	  filesystem

2004-06-14  Bernd Schumacher <bernd.schumacher@hp.com>
	* fixed bootcd2disk/disk2real to support "c0d0p1" disks
	* added bootcd2disk/get_disk to get disk from /proc/partition
	* added TRYFIRST Variable for bootcd2disk
	* start of new version 2.42

2004-06-03  Bernd Schumacher <bernd.schumacher@hp.com>
	* Changed dependencies for package bootcd-mkinitrd

2004-05-27  Bernd Schumacher <bernd.schumacher@hp.com>
	* Added DEVFS=yes Patch from Mark Clarkson
	* No Warning in bootcd2disk if SCRIPT is enabled

2004-05-21  Bernd Schumacher <bernd.schumacher@hp.com>
	* Added bootcdmodprobe to load modules needed with discover and
	discover1

2004-05-19  Carsten Dinkelmann  <din@foobar-cpa.de>
	* Add wildcard DISK to bootcd2disk(.conf) to parametrize partitions
	* Add function after_copy to support some action like remounting after
	the copying

2004-05-17  Bernd Schumacher  <bernd.schumacher@hp.com>
	* Added -s Option for bootcdwrite and bootcd2disk
	* Now only ssh Version which support Option -t are supported by
	bootcd.
	* Changes to bootcdmkinitrd to use new discover options

2004-03-12  Bernd Schumacher  <bernd.schumacher@hp.com>

	* Added -w option for lilo to get less warnings
	* Added -P option to df to be more portable. This prevents
	wraps in long df output.
	* Because sfdisk is not always able to automatically detect
	a disk usable for bootcd2disk, own tests are added
	* Use an easier method instead of "type" to check if mkzftree
	is installed.

2004-02-12  Bernd Schumacher  <bernd.schumacher@hp.com>

	* Fixed uninstallable conflict of bootcd-mkinitrd.

2004-01-24  Bernd Schumacher  <bernd.schumacher@hp.com>

	* fixed bootcd2disk to better detect a DISK automatically
	Newer versions of sfdisk are also listing cdroms now. Because we only
	want to see disks they have to be filtered.

2003-11-18  Bernd Schumacher  <bernd.schumacher@hp.com>

	* Upgraded to Debian Standards-Version: 3.6.0
	* Compresion of ChangeLog
	* Fixed duplicate entry in bootcd.conffile.
	* bootcd now does no longer require to find locatedb.

2003-10-08  Thomas Krennwallner  <krennwallner@aon.at>

	* debian/control: Remove "." in the short description line.

	* debian/README.debian: Fix typo.

2003-10-04  Bernd Schumacher  <bernd.schumacher@hp.com>

	* Announced this ChangeLog in README.debian.

	* Added file debian/docs to install ChangeLog in
	/usr/share/doc/bootcd.

	* Edited debian/changelog and tagged files for Version 2.35.

2003-09-15  Thomas Krennwallner  <krennwallner@aon.at>

	* S12bootcdram.sh: Create /var/log/lastlog at boot-time.
	lastlog(8) works now.

2003-09-01  Thomas Krennwallner  <krennwallner@aon.at>

	* bootcdmkinitrd: Exit if there is no /etc/mkinitrd.

2003-08-31  Thomas Krennwallner  <krennwallner@aon.at>

	* S13bootcdflop.sh: Use filesystem list feature of mount and fsck
	to determine the filesystem used on the FLOPPY. Mount FLOPPY
	read-only and prevent writing to /etc/mtab.

	* FAQ: Fix some typos.

2003-08-26  Bernd Schumacher  <bernd.schumacher@hp.com>

	* FAQ: Add USB memory stick usage.

	* FAQ: Add bootcd mailing list bootcd-user@lists.alioth.debian.org.

	* debian/changelog: Add new version 2.35.

2003-08-25  Thomas Krennwallner  <krennwallner@aon.at>

	* bootcdmkinitrd: Include cdrom.o in initrd (closes alioth #300087).

	* bootcdmkinitrd: Check if /initrd.img is equal to
	boot/initrd.img-$(uname -r) or /boot/initrd.img-$(uname -r).
