==================
 thisbootcd.conf
==================

------------
bootcd utils
------------

:Author: bernd.schumacher@hpe.com
:Date: 2020-08-07
:Copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
:License: GNU General Public License, version 3
:Version: 0.1
:Manual section: 5
:Manual group: bootcd utils

SYNOPSIS
========

/etc/bootcd/thisbootcd.conf

DESCRIPTION
===========

**thisbootcd.conf** is a configuration file, that exists
if the running system is booted from a *bootcd*.
The path is **/etc/bootcd/thisbootcd.conf**.

This file is automatically created, when
the *bootcd* is built and should not be edited.

This file will be sourced as shell file.
The following Options will be included.
The defaults are used by **bootcd2disk**,
if this file does not exist.

OPTIONS
=======

**DISABLE_CRON**

  If this variable is set, the disabled cronjobs will be
  enabled on the new created disk.

  Default::

    DISABLE_CRON=""

**KERNEL**

  This defines the kernel, that will be used on the
  new created disk.

  Default::

    KERNEL=vmlinuz

**INITRD**

  This defines the path to initrd, that will be used on the
  new created disk.

  Default::

    INITRD="initrd.img"

SEE ALSO
========

bootcd(7), bootcdwrite(1), bootcd2disk(1), bootcdflopcp(1),
bootcdmk2diskconf(1), bootcd2disk.conf(5)
