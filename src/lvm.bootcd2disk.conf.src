# lvm.bootcd2disk.conf.src - source for lvm.bootcd2disk.conf
### conf.src conf.start
# vim: set filetype=sh :
#        file: lvm.bootcd2disk.conf
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2001-2020)
#     license: GNU General Public License, version 3
# description: lvm.bootcd2disk.conf - example config for bootcd2disk with
#              logical volume manager (lvm)
#
# Example for 3 disks, lvm and bios boot:
#
# Disk:            +---DISK0-----+ +---DISK1-----+ +DISK2+
# Partition:       DISK0P1 DISK0P2 DISK1P1 DISK1P2 DISK2P1
# Volumegroups:            +----VG00-----+ +----VG01-----+
# Logical Volumes:           LV00   LV01     LV02
# used as:         /boot     swap   /        /usr
#
#
### conf.src bootcd_mount()
bootcd_mount()
{
  bootcd_global DISK0P1
  mount /dev/vg00/lv01 $1
  mkdir -p $1/boot; mount $DISK0P1 $1/boot
  mkdir -p $1/usr; mount /dev/vg01/lv02 $1/usr
}
### conf.src bootcd_umount()
bootcd_umount()
{
  umount $1/usr
  umount $1/boot
  umount $1
}
### conf.src DISK#
DISK0="auto"
DISK1="auto"
DISK2="auto"
### conf.src EFIBOOT
EFIBOOT="bios"
### conf.src EXT4FS
EXT4FS="DISK0P1
/dev/vg00/lv01
/dev/vg01/lv02"
### conf.src FSTAB
FSTAB="\
DISK0P1        /boot ext4 defaults,errors=remount-ro 0 1
/dev/vg00/lv00 none  swap sw 0 0
/dev/vg00/lv01 /     ext4 defaults,errors=remount-ro 0 1
/dev/vg01/lv02 /usr  ext4 defaults,errors=remount-ro 0 1
proc           /proc proc defaults 0 0"
### conf.src LVMGRP
LVMGRP="vg00 DISK0P2 DISK1P1
vg01 DISK1P2 DISK2P1"
### conf.src LVMVOL
LVMVOL="\
lv00 100 vg00
lv01 3000 vg00
lv02 2000 vg01 lvcreate -n lv02 -i 2 -I 8 -L 2000 vg01"
### conf.src SFDISK#
SFDISK0="unit: sectors
,$((100*2048))
,$((3000*2048))" # 100MB, 3GB

SFDISK1="unit: sectors
,$((3000*2048))
,$((3000*2048))" # 3GB,3GB

SFDISK2="unit: sectors
,$((3000*2048))" # 3GB
### conf.src SWAP
SWAP="/dev/vg00/lv00"
### conf.src conf.end
