=============
 bootcd2disk
=============

-------------------------------------------
copy a system running from bootcd to a disk
-------------------------------------------

:Author: bernd.schumacher@hpe.com
:Date: 2020-08-07
:Copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
:License: GNU General Public License, version 3
:Version: 0.1
:Manual section: 1
:Manual group: bootcd utils

SYNOPSIS
========
**bootcd2disk** [**-i|-s|-m**] [**-d** *debug_runtime_config*] -- [**-h|--help**]
  [**-c|--conf** *CONF*] [**-y|--yes**] [**-url** *url*]
  [**--only_mount**] [**--only_unmount**] [**--only_copydata**] [**--only_sync**]
  [**-p|--only_print_options**]]
  [**-o|--use_originaldiskname**] [**-z|--zerodsk**]
  [*--variable_from_bootcd2disk.conf(5)* *value*]
  [*--function_from_bootcd2disk.conf(5)* *definition*]

DESCRIPTION
===========

**bootcd2disk** is used to write a running system to disk.
It was originally used do write a running **bootcd** built with **bootcdwrite**
to disk, but it is also possible to write a system directly to an attached disk.

The system can be copied to one or more disk partitions seen by the running system.
When running from **bootcd**, it is possible to let **bootcd2disk**
automatically find a disk, make partitions on it,
copy the cd to the disk and make the disk bootable.

OPTIONS
=======

**-i|-s|-m|-d** *debug_runtime_config*

  Run bootcdwrite in interactive, silent, minimal control or debug mode.
  See shellia(1) for this standard **shellia** options.

**-c|--conf** *CONF*

  use the bootcdwrite config options from file *CONF*.
  Default::

    CONF=/etc/bootcd/bootcd2disk.conf

**-h|--help**

  print help and exit

**-o|--use_originaldiskname**

  if **bootcd2disk.conf** was created by **bootcdmk2diskconf**, not only the
  variable **DISK0** is defined as **auto** but also **ORIG_DISK0** is
  defined with the original device. To use it this option exists.

**-y|--yes**

  answer always yes

**-url** *url*

  If bootcd2disk is slow on your system (because of a slow CD/DVD drive or the HP ILO virtual CD interface),
  you can use an image server to get the bootcd iso image from.
  The swap partition of the installed system will be used as temporary space for the iso image.

  The image server url is configured with this option.
  Example::

    bootcd2disk -url http://192.168.1.1/cdimage.iso

  Another way the increase the performance is the use of the mounted CD/DVD itself
  for the copy. Example::

    bootcd2disk -url file:///dev/sdc0

  The swap partition of the upcoming system must have enough space to get the whole image!

**--only_mount**

  After *bootcd2disk* did partition, format and install the disk,
  the disks can be mounted again with this option.
  For example to run *update-grub* in chroot on the mounted disk:

    # remount disks to /mnt/bootcd.disc/
    bootcd2disk --only_mount

    # prepare chroot
    mount --bind /dev /mnt/bootcd.disc/dev
    mount -t proc none /mnt/bootcd.disc/proc
    mount -t sysfs none /mnt/bootcd.disc/sys
    mount -t devpts none /mnt/bootcd.disc/dev/pts

    # run *update-grub* in chroot
    chroot /mnt/bootcd.disc update-grub

**--only_unmount**

  If the disks are mounted with option **--only_mount** , they can be unmounted
  with this option.

**--only_copydata**

  This will not create partitions, but only copy data to the directory *COPYDEST*
  defined with **--COPYDEST** *COPYDEST*. Filesystems have to be mounted before.
  The Bootloader will not be activated.

**-p|--only_print_options**

  Print the options calculated from defaults, config-file and commandline
  and exit.

  Hint: This option may display more information about defined functions in a
  non-bash environment, if calling *bash bootcd2disk -p ...*.

**-z|--zerodsk**

  This option writes zeros to the beginning of a disk before using it.

*--variable_from_bootcd2disk.conf(5)* *value*

  variables listed in **bootcd2disk.conf(5)** can be overwritten
  from command line. For example a variable called *VARIABLE* will get the
  value *VALUE* with the option: *--VARIABLE* *VALUE*

*--function_from_bootcd2disk.conf(5)* *definition*

  functions listed in **bootcd2disk.conf(5)** can be overwritten
  from command line. For example a function *fun() { echo "hello world"; }* could
  be defined with the option: *--fun 'fun() { echo "hello world"; }'*

**--only_sync**

  To use this option, **bootcd2disk** should have been run before,
  because the target disk should be partitioned and formatted already.
  With **---only_sync** only the steps needed to sync will be done.
  That means the target disc will be mounted, the files will be synced,
  the bootloader will be activated and the target disk will be unmounted.

ENV
===

All variables, that can be given as *OPTIONS* or in the *bootcd2disk.conf*
file, will not be taken form Environment variables.

Only the following variables will be set as Environment variables.

**LUKS_PASSWD**

  This variable contains the *luks* passphrase. It is used to initialize
  *luks* partitions and to open *luks* partitions. If this variable is
  not set in the *envrionment* it will be asked interactively by
  bootcd2disk if needed.

SEE ALSO
========

bootcd(7), bootcdwrite(1), bootcdflopcp(1), bootcdmk2diskconf(1),
bootcdwrite.conf(5), bootcd2disk.conf(5)
