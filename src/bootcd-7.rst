========
 bootcd
========

------------------
 use bootcd utils
------------------

:Author: bernd.schumacher@hpe.com
:Date: 2020-08-07
:Copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
:License: GNU General Public License, version 3
:Version: 0.1
:Manual section: 7
:Manual group: bootcd utils

SYNOPSIS
========

*bootcd-tool* *options*

*bootcd-tool* = **bootcdwrite** | **bootcd2disk** | **bootcdflopcp**

DESCRIPTION
===========

*bootcd-tools* can copy a running or mounted Debian system.
Different boot configuration are supportet (efi, old bios, secure boot).
**bootcdwrite** creates an ISO life-cd file called **bootcd** using an overlayfs.
**bootcdflopcp** writes changes from overlayfs to floppy disk, when running from **bootcd**.
**bootcd2disk** writes bootable disks (with our without lvm, ext2, ext3, ext4, vfat, swap).

SEE ALSO
========

bootcdwrite(1), bootcd2disk(1), bootcdflopcp(1), bootcdmk2diskconf(1),
bootcdwrite.conf(5), bootcd2disk.conf(5)
