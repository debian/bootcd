==================
 bootcdwrite.conf
==================

------------
bootcd utils
------------

:Author: bernd.schumacher@hpe.com
:Date: 2020-08-07
:Copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
:License: GNU General Public License, version 3
:Version: 0.1
:Manual section: 5
:Manual group: bootcd utils

SYNOPSIS
========

/etc/bootcdwrite.conf

DESCRIPTION
===========

**bootcdwrite.conf** is a configuration file used by **bootcdwrite**.
The default path is **/etc/bootcdwrite.conf**.

This file will be sourced as shell file.
The following Options can be used.
Examples how to use the Options are shown.

OPTIONS
=======

**SRCDISK**

  The Variables **SRCDISK** defines the root of the files
  that will be copied.

  For example, to build an image from a remote system,
  export root-directory with nfs,
  mount it locally to */mnt/remote* and add::

    SRCDISK=/mnt/remote

  It is added as prefix to KERNEL, INITRD, DISABLE_CRON and NOT_TO_CD,
  if this are relativ paths (without starting "/")

  Default::

    SRCDISK=/

**KERNEL**

  This defines the kernel which is used.
  More kernels can be defined with KERNEL<number>
  The numbers have to start with 1 and can continue with 2, 3, 4 ... ::

    KERNEL1=/boot/vmlinuz-2.6.18-3-686-bigmem
    KERNEL2=/boot/vmlinuz-4.19.0-5-amd64

  Default::

    KERNEL=vmlinuz

**APPEND**

  This can be used to define additional options for the kernel
  For more kernels APPEND<number> can be defined::

    APPEND1="vga=normal nomodeset"
    APPEND2=""

  Default::

    APPEND=""

**INITRD**

  This defines the path to initrd.
  For more kernels INITRD<number> can be defined::

    INITRD1=/boot/initrd.img-2.6.18-3-686-bigmem
    INITRD2="initrd.img"

  Default::

    INITRD="initrd.img"

**KLABEL**

  This defines a label for the kernel to be choosen when booting from bootcd.
  Can defines multiple kernels with *KERNEL<number>* and label *KLABEL<number>*::

    KLABEL1=686-3
    KERNEL1=/boot/vmlinuz-2.6.18-3-686-bigmem
    INITRD1=/boot/initrd.img-2.6.18-3-686-bigmem
    APPEND1=

  Default::

    KLABEL=linux

**DISPLAY**

  Text files to display at boottime (see syslinux doku)
  When pressing **F1** at boot time the file defined by *DISPLAY* is shown.
  **F2** will show the file defined by *DISPLAY2* and **F3**
  will show the file defined by *DISPLAY3*
  **F10** lists all people mentioned in *changelog*.

  The text file *kernelinfo* is replaced with a file which includes kernelinfo.

  Default::

    DISPLAY="/usr/share/bootcd/default.txt"
    DISPLAY2="kernelinfo"


**TYP**

  TYP can be defined as **CD** or **DVD** to get a warning if the size of
  the image will be larger as the specified medium.

  Default::

    TYP=DVD

**CDDEV**

  Specifies one or more CD devices to boot from. The first one is the default.
  **auto** tries to find the bootcd on all SCSI and IDE CDROMS

  Default::

    CDDEV="auto /dev/hda /dev/hdb /dev/hdc /dev/hdd /dev/sr0 /dev/sr1"

**DO_CHECK**

  Enables some checks or not.
  To check for possible problems can take a long time, but it is recommended to do the checks.

  Default::

    DO_CHECK=yes

**NOT_TO_CD**

  This variable can also contain special characters. See **EVALVARS**.

  Add directories which should be excluded from bootcd.
  Directories are separated by spaces.
  SRCDISK will be automatically added to each path that does not start with "/".

  Default::

    NOT_TO_CD=""

**SSHHOSTKEY**

  If using ssh it is helpful to have a unique ssh hostkey for each CD.
  To be generated at burntime::
  Default::

    SSHHOSTKEY=yes

**UDEV_FIXNET**

  If the network interfaces are hardwired in
  /etc/udev/rules.d/xx_persistent-net.rules
  they can removed on bootcd, with:
  Default::

    UDEV_FIXNET="yes"

**VAR**

  This defines the directoy where the image resists after build.
  Default::

    VAR=/var/spool/bootcd

**FLOPPY_RUNTIME_DEV**

  When you booting from cd changes will be read from this device.
  Without a floppy::

    FLOPPY_RUNTIME_DEV=""

  Default::

    FLOPPY_RUNTIME_DEV=/dev/fd0

**BOOTCDFLOPPY**

  If the system should boot the kernel from floppy.
  This reduces space on floppy used by bootcdflopcp.
  For this to work FLOPPY_CREATE_DEV has to be specified::

    BOOTFLOPPY=yes|no

  Default::

    BOOTFLOPPY=no

**BOOT_ONLY_WITH_FLOPPY**

  If you several machines will be booted from the same bootcd, you must have
  each one can hav separat configuraiton (exp: /etc/network/interfaces) on floppy.
  If the floppy can not be mounted, it may be a good idea to stop booting with::

    BOOT_ONLY_WITH_FLOPPY=yes

  Default::

    BOOT_ONLY_WITH_FLOPPY=no

**CLEAN_VAR**

  delete some chached files in /var with:
  Default::

    CLEAN_VAR=yes

**TO_FSTAB**

  To add addiditionel entries to fstab. Use for example::

    TO_FSTAB="/dev/hdc1 /home ext3 defaults 1 1

  Default::

    TO_FSTAB=""

**NOTCOMPRESSED**

  Files or Directory-Trees that should never be compressed on CD can be listed here.
  This can be used for documentation which should be visible without installing the bootcd.
  Default::

    NOTCOMPRESSED=""

**DISABLE_CRON**

  Files listed will be on the cdrom with a .no_run_on_bootcd suffix so run-parts won't execute them.
  The original file will be a link to /bin/true.
  This is meant for cron maintenance scripts which are useless on a static cdrom.
  The default is to disable the daily find, standard and security scripts.
  With bootcd2disk this files will be reactivated again.

  Default::

    DISABLE_CRON="etc/cron.daily/find etc/cron.daily/standard etc/cron.daily/security"

**MKISOFS_CHNG**

  With this variable some options given to mkisofs by bootcdwrite can be changed.

  Example: To use *-opt1* and *-opt2 <value>* and
  to not use any previous *-opt1 [<value>...]*, *-opt2 [<value...>]* and
  *-opt3 [<value>...]* and to leave any other options as defined::

    MKISOFS_CHNG="-opt1 -opt2 <value> !-opt3"

  Default::

    MKISOFS_CHNG=""


**extra_changes()**

  It is possible to define a function called extra_changes to have some
  files modified on the ISO image. Here is an example::

    extra_changes() {
      bootcd_global VAR

      echo "noname" >$VAR/changes/etc/hostname

      mkdir -p $VAR/changes/etc/network
      ( echo "auto lo"
        echo "iface lo inet loopback"
        echo "" echo "auto eth0"
        echo "iface eth0 inet static"
        echo "       address 0.0.0.0"
        echo "       netmask 255.255.255.0"
      ) >$VAR/changes/etc/network/interfaces

      echo "127.0.0.1 localhost noname" >$VAR/changes/etc/hosts

      cat $SRCDISK/etc/passwd |
      grep -v -e "^bs:" -e "^bianca:" -e "^tim:" >$VAR/changes/etc/passwd

      cat $SRCDISK/etc/shadow |
      grep -v -e "^bs:" -e "^bianca:" -e "^tim:" >$VAR/changes/etc/shadow

      cat $SRCDISK/etc/group |
      grep -v -e "^bs:" -e "^bianca:" -e "^tim:" >$VAR/changes/etc/group
    }

  Default::

    # make sure the function is not defined
    unset -f extra_changes

**BOOTCDMODPROBE**

  If booted from initrd bootcd has to load the necessary modules.
  If only modules provided by initramfs-tools are needed you can
  specify "standard" here::

    BOOTCDMODPROBE=standard

  If bootcd should try extra hard to load
  neccessary modules you can specify "bootcd" here.
  Bootcd will use discover for this purpose. So discover has to be installed::

    BOOTCDMODPROBE=bootcd

  If you specify auto, bootcd will check if discover is installed.
  If it is installed BOOTCDMODPROBE=bootcd will be set, if not
  BOOTCDMODPROBE=standard will be set.
  Be aware that people have reported, that sometimes BOOTCDMODPROBE=bootcd
  may not work but sometimes it is needed.
  Default::

    BOOTCDMODPROBE=auto

EVALVARS
========

  Most variables listed in chapter **OPTIONS** can't contain special
  characters.

  It is not recommended to use special characters anyway, but
  the variable **NOT_TO_CD**
  may contain blank ( ), dollar ($), backslash (\\), qoute (") and
  backquote (`). This variables are called **EVALVARS** in this manpage,
  because in shell script, the *eval* command can be used to easily read
  them.

  For example to define **NOT_TO_CD** with the files or directories
  "/etc/noblank", "/etc/blank name1" and "/etc/blank name2" the
  following config line would be possible::

    NOT_TO_CD="/etc/noblank \"/etc/blank name1\" /etc/blank\ name2"

  Because all variables have to be readable by shell with the *eval* command,
  the syntax can be checked. if the config line above is entered in a shell
  with the test commands::

    eval "set -- $NOT_TO_CD"
    echo "#=<$#> 1=<$1> 2=<$2> 3=<$3> 4=<$4>"

  should print out the result::

    #=<3> 1=</etc/noblank> 2=</etc/blank name1> 3=</etc/blank name2> 4=<>

ENVIRONMENT
===========

**ia_logfile**

  The logfile of *bootcdwrite* is */var/log/bootcdwrite*,
  if not overwriten with variable *ia_logfile* before.

Using functions
===============

  Used functions listed in *OPTIONS* should declare all
  used variables listed in *OPTIONS* with the function
  **bootcd_global <var1> [...]**.

  For example a function **extra_changes()** that wants to
  use variable **VAR** should start with::

    extra_changes()
    {
      bootcd_global VAR

  see full example in **OPTIONS/extra_changes()**

  The function **bootcd_global** makes sure that all given
  variables are declared.

SEE ALSO
========

bootcd(7), bootcdwrite(1), bootcd2disk(1), bootcdflopcp(1),
bootcdmk2diskconf(1), bootcd2disk.conf(5)
