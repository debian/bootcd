=============
 bootcdwrite
=============

------------------------------------------------
build a bootable iso image from a running system
------------------------------------------------

:Author: bernd.schumacher@hpe.com
:Date: 2020-08-07
:Copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
:License: GNU General Public License, version 3
:Version: 0.1
:Manual section: 1
:Manual group: bootcd utils

SYNOPSIS
========
**bootcdwrite** [**-i|-s|-m**] [**-d** *debug_runtime_config*] -- [**-h|--help**]
  [**-c|--conf** *CONF*] [**--only_floppy**] [**-y|--yes**] [**--mokcrtder** *MOKCRTDER*]
  [*--variable_from_bootcdwrite.conf(5)* *value*]
  [*--function_from_bootcdwrite.conf(5)* *definition*]


DESCRIPTION
===========

**bootcdwrite** is used to write a Linux Installation to an iso image.
It is possible to copy a system mounted per NFS to a local iso.
The iso can be copied to a CD or DVD with standard tools.

It is important that the kernel can access the cdrom on the system, where
you want to boot from the CD, without having to load any modules. (Because to
load modules from CD, the kernel must have access to CD before). If this
is not the case you have first to create a new kernel with CD-support
built in.

If something goes wrong with **bootcdwrite**, the user will be given the chance
to correct the problem. bootcdwrite starts many commands. If a command exits
with an exit code != 0, or if a command produces unknown output on stdout or
stderr, the command and the output will be shown to the user.
He gets the option to exit, retry the failed command, or to ignore the failed
command.

OPTIONS
=======

**-i|-s|-m**

  Run bootcdwrite in interactive, silent or minimal control mode.
  See shellia(1) for this standard **shellia** options.

**-d** *debug_runtime_config*

  Run bootcdwrite in debug mode.
  See shellia(1) for this standard **shellia** options.

**-c|--conf** *CONF*

  use the bootcdwrite config options from file *CONF*.
  Default::

    CONF=/etc/bootcd/bootcdwrite.conf

**-h|--help**

  print help and exit

**--only_floppy**

  only a bootfloppy (to boot an existing bootcd) has to be created.

**-y|--yes**

  answer always yes

**--mokcrtder** *MOKCRTDER*

  If an own *Machine Owner Key* (*MOK*) is needed to boot the created bootcd,
  with *secure boot* enabled, this option can install the public *MOK*
  Certificate in *DER* format (*MOKCRTDER*) to the *EFI* partition of the bootcd,
  to be loaded manually in *BIOS*. See https://wiki.debian.org/SecureBoot.

  If *secure boot* is not used, or if a standard Debian signed Kernel is used
  on a system with the standard Microsoft Platform Key (*PK*) installed in *BIOS*
  this option is not needed.

*--variable_from_bootcdwrite.conf(5)* *value*

  variables or functions listed in **bootcdwrite.conf(5)** can be overwritten
  from command line. For example a variable called *VARIABLE* will get the
  value *VALUE* with the option: *--VARIABLE* *VALUE*

*--function_from_bootcd2disk.conf(5)* *definition*

  functions listed in **bootcdwrite.conf(5)** can be overwritten
  from command line. For example a function *fun() { echo "hello world"; }* could
  be defined with the option: *--fun 'fun() { echo "hello world"; }'*

SEE ALSO
========

bootcd(7), bootcd2disk(1), bootcdflopcp(1), bootcdmk2diskconf(1),
bootcdwrite.conf(5), bootcd2disk.conf(5)
