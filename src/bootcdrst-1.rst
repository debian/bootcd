===========
 bootcdrst
===========

---------------------------------------------
development tool to use info from rst sources
---------------------------------------------

:Author: bernd.schumacher@hpe.com
:Date: 2020-08-07
:Copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
:License: GNU General Public License, version 3
:Version: 0.1
:Manual section: 1
:Manual group: bootcd utils

SYNOPSIS
========

**bootcdrst** **--run-self-test**|*target file*
*target_file*=**bootcd2disk.conf**|**bootcdconf.lib**|**bootcdwrite.conf**

DESCRIPTION
===========

**bootcdrst** is only an internal tool used in build process.

OPTIONS
=======

**--run-self-test**

  Run a test for each function.

  This option is only needed in develpment.

*target file*

  **bootcdrst** can create some files needed by **bootcd**.
  This option creates *target_file*.
  This option is used by Makefile at build time.

SEE ALSO
========

bootcd(7), bootcdwrite(1), bootcd2disk(1), bootcdflopcp(1),
bootcdmk2diskconf(1), bootcdwrite.conf(5), bootcd2disk.conf(5)
